<?php

namespace SH\Package\Tests;

use SH\Package\Service;

class ServiceTest extends AbstractTestCase {

    public function testNameCanBeSetAndRetrieved() {
        $app = new Service();
        $app->setName('John Doe');

        $this->assertSame('John Doe', $app->getName());
    }

}
